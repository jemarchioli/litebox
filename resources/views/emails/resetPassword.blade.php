Hi there,<br><br>
your new password is: <b>{{$newPassword}}</b>
<br><br>
<b>Please follow the link below to confirm the update:</b><br>
<a href="{{ route('confirm_password') }}/{{ $token }}">{{ route('confirm_password') }}/{{ $token }}</a>