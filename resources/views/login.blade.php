@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="access-panel">
                <div class="access-panel-header">
                    <a href="javascript:void(0);" class="close-btn"></a>
                    <h1>
                        LOGIN
                    </h1>
                    <div class="middle-row">
                        <div class="lines"></div>
                        <span>TO</span>
                        <div class="lines"></div>
                    </div>
                    <h4>
                        YOUR ACCOUNT
                    </h4>
                </div>
                <div class="access-panel-body">
                    <login></login>
                    <a href="javascript:void(0);" class="forgot-password">
                        FORGOT PASSWORD?
                    </a>
                </div>
            </div>
            <div class="access-panel second-row">
                <div class="rope left-side"></div>
                <div class="rope right-side"></div>
                <a href="javascript:void(0);" class="btn btn-default btn-social">Login with <i class="fa fa-bitbucket" aria-hidden="true"></i></a>
                <a href="javascript:void(0);" class="btn btn-default btn-social m-l">Login with <i class="fa fa-facebook" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</div>
@endsection
