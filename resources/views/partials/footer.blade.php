<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-push-4 col-sm-4">
                <img src="images/potluck-logo-footer@2x.png" class="logo-img" srcset="images/potluck-logo-footer@2x.png 2x">
            </div>
            <div class="col-xs-12 col-sm-push-4 col-sm-4">
                <ul class="contact-info clearfix">
                    <li>
                        <a href="mailto:info@potluck.us">info@potluck.us</a>
                    </li>
                    <li>
                        <a href="tel:1 888.221.8187">1 888.221.8187</a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-pull-8 col-sm-4">
                <p>
                    2017 &copy; Potluck by CookUnity, all rights reserved.
                </p>
            </div>

        </div>
    </div>
</footer>