/**
 *
 * Created by Juan on 7/31/2017.
 */
import axios from 'axios';

var axiosInstance = axios.create({
    // baseURL: 'http://litebox.local/api/v1',
    baseURL: 'http://litebox.wedoweb.co/api/v1',
    /* other custom settings */
});

export default axiosInstance;