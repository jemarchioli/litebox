import axiosInstance from '../api';
import router from '../router';

export default {
    user: {
        loading: false,
        authenticated: true,
        api_token: false,
        info: {}
    },
    initialize() {
        console.log('auth initialize');

        var self = this
        return new Promise(function(resolve, reject) {
            self.checkAuth().then(()=> resolve(), ()=> resolve())

        })
    },
    checkAuth() {
        console.log('checkAuth');

        var self = this
        var jwt = localStorage.getItem('api_token')
        var userData = JSON.parse(localStorage.getItem('user_data'))
        // var userData = ''
        // console.log(jwt);

        return new Promise(function(resolve, reject) {
            axiosInstance.post('checkToken', {'token': jwt})
                .then((resp)=> {
                    self.setUser(resp.data.data, userData);

                    resolve();
                })
                .catch(()=>{
                    self.user.authenticated = false;
                    reject();
                })
        })
    },
    setUser(token, userData) {
        this.user.loading = false;
        this.user.api_token = token;
        this.user.authenticated = true;
        this.user.info = userData;

        localStorage.setItem('api_token', token);
        localStorage.setItem('user_data', JSON.stringify(userData));
    },
    doLogin(context, credentials, redirect) {
        axiosInstance.post('login', credentials)
            .then((resp) => {
                this.setUser(resp.data.data.token, resp.data.data.user);

                router.push(redirect);
            })
            .catch((error) => {
                context.hasError = true;
                context.error = error.response.data.error.message
            })
    },
    doLogout() {
        localStorage.removeItem('api_token')
        localStorage.removeItem('user_data')
        this.user.api_token = false
        this.user.authenticated = false
        this.user.data = {}
    }
}