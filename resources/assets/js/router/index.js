/**
 * Created by Juan on 7/30/2017.
 */
import Vue from 'vue'
import VueRouter from 'vue-router'
import auth from '../auth'
import Home from '../components/Home.vue'
import Login from '../components/Login.vue'
import ForgotPassword from '../components/Forgot-password.vue'
import Logout from '../components/Logout.vue'
import axiosInstance from '../api';



Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: Home
        },
        {
            path: '/logout',
            name: 'logout',
            component: Logout
        },
        {
            path: '/login',
            component: Login,
            meta: {
                guest: true
            }
        },{
            path: '/forgot-password',
            component: ForgotPassword,
            meta: {
                guest: true
            }
        },
        {
            path: '/login/callback',
            meta: {
                guest: true
            },
            beforeEnter: (to, from, next)=> {
                auth.user.loading = true;
                axiosInstance.post('login/bitbucket/code', to.query)
                    .then((resp) => {
                        auth.setUser(resp.data.data.token, resp.data.data.user);
                        next('/')
                    })
                    .catch((error) => {
                        console.log(error.response)
                        next('/login');
                    })
            }
        },
        {
            path: '/login/fbcallback',
            meta: {
                guest: true
            },
            beforeEnter: (to, from, next)=> {
                auth.user.loading = true;
                axiosInstance.post('login/facebook/code', to.query)
                    .then((resp) => {
                    console.log(resp);
                        auth.setUser(resp.data.data.token, resp.data.data.user);
                        next('/')
                    })
                    .catch((error) => {
                        console.log(error.response)
                        next('/login');
                    })
            }
        }
    ]
})

router.beforeEach((to, from, next) => {
    if( to.meta.guest ) {
        next();
    }
    else if( auth.user.authenticated ){
        auth.checkAuth()
            .then(()=> {
                next();
            },
            ()=> {
                next('/login');
            })
    }
    else {
        next('/login');
    }

})
export default router