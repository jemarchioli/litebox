<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'namespace' => 'Api\V1'], function() {
    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::post('/checkToken', ['uses' => 'LoginController@checkToken']);

        Route::post('/test', function() {
            return "test";
        });
    });

    Route::post('/login', ['uses' => 'LoginController@index']);
    Route::get('/login/bitbucket', ['uses' => 'LoginController@bitbucket']);
    Route::get('/login/facebook', ['uses' => 'LoginController@facebook']);
    Route::post('/login/bitbucket/code', ['uses' => 'LoginController@bitbucketCallback']);
    Route::post('/login/facebook/code', ['uses' => 'LoginController@facebookCallback']);
    Route::post('/forgot-password', ['uses' => 'LoginController@forgotPassword']);
    Route::post('/create', ['uses' => 'LoginController@create']);
});
