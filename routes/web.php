<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => '/', 'namespace' => 'Web'], function() {
    /*Route::group(['middleware' => 'auth'], function () {
        Route::get('/', function () {
            return view('welcome');
        });
    });

    Route::get('/login', ['as' => 'login', 'uses' => 'LoginController@index']);
    Route::get('/recovery', ['as' => 'password.request', 'uses' => function() {
        echo "recovery page";
    }]);
    Route::get('/register', ['as' => 'register', 'uses' => function() {
        echo "register page";
    }]);

    Route::get('/login/callback', ['as' => 'login.callback', 'uses' => 'LoginController@callback']);*/
    Route::get('/confirm-password/{token?}', ['as' => 'confirm_password', 'uses' => 'LoginController@confirmPassword']);
    Route::get('/{any}', function () {
        return view('welcome');
    })->where(['any' => '.*']);
});