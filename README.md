# README #

Litebox test

### Requirements ###

- Node 6.11
- NPM 3.10
- Apache
- Mysql
- PHP 7.1+
- Composer

### Quickstart ###

```sh
$ git clone https://@bitbucket.org/jemarchioli/litebox.git
$ cd litebox
$ npm install
$ composer install
```
Copy/paste .env.example as .env and add your local db credentials.

Set up a local host as 'http://litebox.local'

### Compiling assets ###
```sh
$ npm run dev
$ npm run watch
```
