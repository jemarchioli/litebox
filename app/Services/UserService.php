<?php
/**
 * Created by PhpStorm.
 * User: Juan
 * Date: 7/28/2017
 * Time: 12:22 PM
 */

namespace Marchioli\Litebox\Test\Services;


use Illuminate\Support\Facades\Hash;
use Marchioli\Litebox\Test\User;
use Marchioli\Litebox\Test\UserResetPassword;

class UserService
{

    public function __construct(User $user)
    {
        $this->model = $user;
    }

    public function store($userData) {

        $user = $this->model->create($userData);

        return $user;
    }

    public function findByEmail($email) {
        $user = $this->model->where('email', '=', $email)->first();

        return $user;
    }

    public function findOrCreate($user) {
        $userExists = $this->findByEmail($user->email);
        if( !$userExists ) {
            $userData['email'] = $user->email;
            $userData['password'] = Hash::make($user->id);

            return $this->store($userData);
        }
        else {
            $userExists->password = Hash::make($user->id);
            $userExists->save();
        }
        return $userExists;
    }

    public function resetPassword($data) {
        $reset = new UserResetPassword();
        $reset->token = $data['token'];
        $reset->newPassword = Hash::make($data['newPassword']);
        $reset->user_id = $data['user_id'];

        $reset->save();

        return true;
    }
}