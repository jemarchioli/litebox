<?php

namespace Marchioli\Litebox\Test\Http\Middleware;

use Closure;
use Marchioli\Litebox\Test\Exceptions\TokenRequiredException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use JWTAuth;

class VerifyJWTToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::toUser($request->input('token'));
        }catch (JWTException $e) {
            if($e instanceof TokenExpiredException) {
                throw new \Marchioli\Litebox\Test\Exceptions\TokenExpiredException();
            }
            else if($e instanceof TokenInvalidException) {
                throw new \Marchioli\Litebox\Test\Exceptions\TokenInvalidException();
            }
            else if($e instanceof JWTException){
                throw new TokenRequiredException();
            }
        }
        return $next($request);
    }
}
