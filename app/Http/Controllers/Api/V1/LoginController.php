<?php

namespace Marchioli\Litebox\Test\Http\Controllers\Api\V1;

use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailer;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;
use Marchioli\Litebox\Test\Exceptions\CredentialsInvalidException;
use Marchioli\Litebox\Test\Exceptions\EmailNotFoundException;
use Marchioli\Litebox\Test\Exceptions\InvalidGrantException;
use Marchioli\Litebox\Test\Exceptions\ParamsRequiredException;
use Marchioli\Litebox\Test\Exceptions\TokenInvalidException;
use Marchioli\Litebox\Test\Http\Controllers\Api\Controller;
use Marchioli\Litebox\Test\Services\UserService;
use Marchioli\Litebox\Test\User;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, UserService $userService)
    {
        $credentials = $request->only('email', 'password');

        $token = $this->jwtToken($credentials);


        $user = $userService->findByEmail($credentials['email']);

        return $this->response(compact(['token','user']));

    }

    public function checkToken(Request $request)
    {
        $param = $request->only('token');
        $token = JWTAuth::refresh($param['token']);

        return $this->response($token);
    }

    public function bitbucket()
    {
        $url = Socialite::driver('bitbucket')->stateless()->redirect()->getTargetUrl();
        return $this->response($url);
    }

    public function facebook()
    {
        $url = Socialite::driver('facebook')->stateless()->redirect()->getTargetUrl();
        return $this->response($url);
    }

    public function facebookCallback(Request $request, UserService $userService) {
        try {
            $socialUser = Socialite::driver('facebook')->stateless()->user();
            $user = $userService->findOrCreate($socialUser);
            $credentials = array(
                'email' => $user->email,
                'password' => $socialUser->id
            );
        }
        catch(RequestException $e) {
            throw new InvalidGrantException();
        }

        $token = $this->jwtToken($credentials);

        return $this->response(compact('token', 'user'));
    }

    private function jwtToken($credentials) {
        try {
            if(!$token = JWTAuth::attempt($credentials)) {
                throw new CredentialsInvalidException();
            }
        }catch (JWTException $e) {
            return response()->json(['failed_to_create_token'], 500);
        }

        return $token;
    }

    public function bitbucketCallback(Request $request, UserService $userService) {
        try {
            $socialUser = Socialite::driver('bitbucket')->stateless()->user();
            $user = $userService->findOrCreate($socialUser);
            $credentials = array(
                'email' => $user->email,
                'password' => $socialUser->id
            );
        }
        catch(RequestException $e) {
            throw new InvalidGrantException();
        }

        $token = $this->jwtToken($credentials);


        return $this->response(compact('token', 'user'));
    }

    public function forgotPassword(Request $request, UserService $userService, Mailer $mailer) {
        $params = $request->only(['email', 'newPassword']);

        if(!$params['email'] || !$params['newPassword']) {
            throw new ParamsRequiredException();
        }
        try {
            $user = User::select()->where('email', $params['email'])->firstOrFail();
        }catch(\Exception $e) {
            throw new EmailNotFoundException();
        }

        $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        $params['token'] = substr( str_shuffle( $chars ), 0, 8 );
        $params['user_id'] = $user->id;

        if( $userService->resetPassword($params) ) {
            $email = $params['email'];
            $newPassword = $params['newPassword'];
            $token = $params['token'];

            $mailer->send('emails.resetPassword', compact('newPassword', 'token'),
                function($mail) use($email){
                    $mail->from(env('MAIL_FROM', 'no-reply@wedoweb.co'), 'Litebox - Potluck');
                    $mail->to($email);

                    $mail->subject('Potluck: Reset password confirmation');
                });
        }

        return $this->response(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, UserService $userService)
    {
        $data = $request->only('email', 'password');;
        $data['password'] = Hash::make($data['password']);

        $user = $userService->store($data);

        return $this->response($user);
    }

}
