<?php

namespace Marchioli\Litebox\Test\Http\Controllers\Api;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Support\Facades\Response;
use Marchioli\Litebox\Test\Http\Controllers\Controller as BaseController;
/**
 * Created by PhpStorm.
 * User: Juan
 * Date: 7/28/2017
 * Time: 2:11 AM
 */
abstract class Controller extends BaseController
{
    /**
     * Generates a wrapped JSON response in use for the api
     *
     * @param null $content
     * @param int $code
     * @param null $meta
     * @return ResponseFactory|Response
     */
    public function response($content = null, $code = 200, $meta = null)
    {
        return response()->make($this->wrap($content, $meta), $code);
    }

    /**
     * Generates a raw JSON response in use for the api
     *
     * @param null $content
     * @param int $code
     * @return mixed
     */
    public function raw($content = null, $code = 200) {
        return response()->make($content, $code);
    }

    /**
     * Wraps the response using a given array format
     *
     * @param null $response
     * @param null $meta
     * @return array
     */
    public function wrap($response = null, $meta = null)
    {
        if(is_array($meta)) {
            return [
                'data' => $response,
                'meta' => $meta
            ];
        }

        return [
            'data' => $response
        ];
    }
}