<?php

namespace Marchioli\Litebox\Test\Http\Controllers\Web;

use Illuminate\Http\Request;
use Marchioli\Litebox\Test\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use Marchioli\Litebox\Test\UserResetPassword;

class LoginController extends Controller
{
    public function index() {
        return view('login');
    }

    public function confirmPassword($token = '') {

        $reset = UserResetPassword::where('token', $token)->first();

        $error = ['success' => true, 'message' => 'Your new password has been set successfully.'];
        if( !$token || !$reset )
        {
            $error = ['success' => false, 'message' => 'Invalid or expired password reset token.'];

            return redirect('/login')->with('resetResponse', $error);
        }

        $userId = $reset->user->id;
        $reset->user->password = $reset->newPassword;
        $reset->user->save();

        $cleanReset = UserResetPassword::where('user_id', $userId)->delete();

        return redirect('/login')->with('resetResponse', $error);

    }

    public function callback(Request $request) {
        $code = $request->only('code');
//        $user = Socialite::driver('bitbucket')->stateless()->user();

        return response()->json(['data' => $code]);
    }
}
