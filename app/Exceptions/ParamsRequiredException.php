<?php
/**
 * Created by PhpStorm.
 * User: Juan
 * Date: 7/28/2017
 * Time: 2:11 PM
 */

namespace Marchioli\Litebox\Test\Exceptions;


class ParamsRequiredException extends APIException
{
    /**
     * Human readable message
     * @var string
     */
    protected $message = 'Email and Password are required.';

    /**
     * Internal reference code
     * @var string
     */
    protected $internal = 'MISSING_PARAMS';

    /**
     * HTTP Status Code
     * @var int
     */
    protected $statusCode = 400;
}