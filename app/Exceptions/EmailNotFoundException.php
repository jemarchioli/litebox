<?php
/**
 * Created by PhpStorm.
 * User: Juan
 * Date: 7/28/2017
 * Time: 1:57 AM
 */

namespace Marchioli\Litebox\Test\Exceptions;


class EmailNotFoundException extends APIException
{
    /**
     * Human readable message
     * @var string
     */
    protected $message = 'Email not found.';

    /**
     * Internal reference code
     * @var string
     */
    protected $internal = 'EMAIL_NOT_FOUND';

    /**
     * HTTP Status Code
     * @var int
     */
    protected $statusCode = 404;
}