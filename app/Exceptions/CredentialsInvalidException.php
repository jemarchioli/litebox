<?php
/**
 * Created by PhpStorm.
 * User: Juan
 * Date: 7/28/2017
 * Time: 2:11 PM
 */

namespace Marchioli\Litebox\Test\Exceptions;


class CredentialsInvalidException extends APIException
{
    /**
     * Human readable message
     * @var string
     */
    protected $message = 'Invalid email and/or password';

    /**
     * Internal reference code
     * @var string
     */
    protected $internal = 'CREDENTIALS_INVALID';

    /**
     * HTTP Status Code
     * @var int
     */
    protected $statusCode = 401;
}