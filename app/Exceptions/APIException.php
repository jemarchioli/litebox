<?php
/**
 * Created by PhpStorm.
 * User: Juan
 * Date: 7/28/2017
 * Time: 1:55 AM
 */

namespace Marchioli\Litebox\Test\Exceptions;


abstract class APIException extends \Exception
{
    /**
     * Human readable message
     * @var string
     */
    protected $message;

    /**
     * Internal reference code
     * @var string
     */
    protected $internal;

    /**
     * HTTP Status Code
     * @var int
     */
    protected $statusCode;

    /**
     * @param string $message
     * @param string $internal
     * @param int $statusCode
     * @param \Exception|null $previous
     */
    public function __construct($message = null,
                                $internal = null,
                                $statusCode = 0,
                                \Exception $previous = null)
    {
        parent::__construct($this->message ?: $message, $this->statusCode ?: $statusCode, $previous);
        $this->message = $this->message ?: $message;
        $this->internal = $this->internal ?: $internal;
        $this->statusCode = $this->statusCode ?: $statusCode;
    }

    /**
     * Renders the exception with a predefined format
     *
     * @return array
     */
    public function render()
    {
        return [
            'error' => [
                'message' => $this->message,
                'identifier' => $this->internal,
                'code' => $this->statusCode,
            ]
        ];
    }

    /**
     * Returns the internally stored status code
     *
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }
}