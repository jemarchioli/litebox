<?php
/**
 * Created by PhpStorm.
 * User: Juan
 * Date: 7/28/2017
 * Time: 2:52 PM
 */

namespace Marchioli\Litebox\Test\Exceptions;


class TokenInvalidException extends APIException
{
    /**
     * Human readable message
     * @var string
     */
    protected $message = 'Token invalid';

    /**
     * Internal reference code
     * @var string
     */
    protected $internal = 'TOKEN_INVALID';

    /**
     * HTTP Status Code
     * @var int
     */
    protected $statusCode = 401;
}