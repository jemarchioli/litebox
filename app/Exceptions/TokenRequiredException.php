<?php
/**
 * Created by PhpStorm.
 * User: Juan
 * Date: 7/28/2017
 * Time: 2:11 PM
 */

namespace Marchioli\Litebox\Test\Exceptions;


class TokenRequiredException extends APIException
{
    /**
     * Human readable message
     * @var string
     */
    protected $message = 'Token is required';

    /**
     * Internal reference code
     * @var string
     */
    protected $internal = 'TOKEN_REQUIRED';

    /**
     * HTTP Status Code
     * @var int
     */
    protected $statusCode = 401;
}