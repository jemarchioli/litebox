<?php
/**
 * Created by PhpStorm.
 * User: Juan
 * Date: 7/28/2017
 * Time: 2:11 PM
 */

namespace Marchioli\Litebox\Test\Exceptions;


class InvalidGrantException extends APIException
{
    /**
     * Human readable message
     * @var string
     */
    protected $message = 'The specified code is not valid';

    /**
     * Internal reference code
     * @var string
     */
    protected $internal = 'INVALID_GRANT';

    /**
     * HTTP Status Code
     * @var int
     */
    protected $statusCode = 401;
}