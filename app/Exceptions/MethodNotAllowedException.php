<?php
/**
 * Created by PhpStorm.
 * User: Juan
 * Date: 7/28/2017
 * Time: 2:20 AM
 */

namespace Marchioli\Litebox\Test\Exceptions;


class MethodNotAllowedException extends APIException
{
    /**
     * Human readable message
     * @var string
     */
    protected $message = 'The method is not allowed on this request.';

    /**
     * Internal reference code
     * @var string
     */
    protected $internal = 'METHOD_NOT_ALLOWED';

    /**
     * HTTP Status Code
     * @var int
     */
    protected $statusCode = 405;
}