<?php
/**
 * Created by PhpStorm.
 * User: Juan
 * Date: 7/28/2017
 * Time: 3:47 PM
 */

namespace Marchioli\Litebox\Test\Exceptions;


class TokenExpiredException extends APIException
{
    /**
     * Human readable message
     * @var string
     */
    protected $message = 'Token expired';

    /**
     * Internal reference code
     * @var string
     */
    protected $internal = 'TOKEN_EXPIRED';

    /**
     * HTTP Status Code
     * @var int
     */
    protected $statusCode = 401;
}