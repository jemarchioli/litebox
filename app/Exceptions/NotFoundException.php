<?php
/**
 * Created by PhpStorm.
 * User: Juan
 * Date: 7/28/2017
 * Time: 1:57 AM
 */

namespace Marchioli\Litebox\Test\Exceptions;


class NotFoundException extends APIException
{
    /**
     * Human readable message
     * @var string
     */
    protected $message = 'The requested route has not been found.';

    /**
     * Internal reference code
     * @var string
     */
    protected $internal = 'HTTP_NOT_FOUND';

    /**
     * HTTP Status Code
     * @var int
     */
    protected $statusCode = 404;
}