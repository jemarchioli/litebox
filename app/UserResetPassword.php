<?php

namespace Marchioli\Litebox\Test;

use Illuminate\Database\Eloquent\Model;

class UserResetPassword extends Model
{
    protected $table = 'user_reset_password';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'token', 'newPassword'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'newPassword'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
